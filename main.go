package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/websocket"
	"github.com/graphql-go/graphql"
	"github.com/graphql-go/handler"
	"log"
	"net/http"
	"sync"
)

type Player struct {
	Id    string
	Name  string
	Score int
}

type GraphQLIncomingRequest struct {
	Query         string                 `json:"query" url:"query" schema:"query"`
	Variables     map[string]interface{} `json:"variables" url:"variables" schema:"variables"`
	OperationName string                 `json:"operationName" url:"operationName" schema:"operationName"`
}

type ConnectionACKMessage struct {
	OperationID string `json:"id,omitempty"`
	Type        string `json:"type"`
	Payload     struct {
		Query string `json:"query"`
	} `json:"payload,omitempty"`
}

type Subscriber struct {
	ID            int
	Conn          *websocket.Conn
	RequestString string
	OperationID   string
}

var players []Player = []Player{Player{Id: "1", Name: "Asif", Score: 42}, {Id: "2", Name: "Sifat", Score: 89}}
var publishingChannel = make(chan string, 10)

func main() {
	var upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
		Subprotocols: []string{"graphql-ws"},
	}

	var subscribers sync.Map

	playerType := graphql.NewObject(graphql.ObjectConfig{
		Name: "Player",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.String,
			},
			"name": &graphql.Field{
				Type: graphql.String,
			},
			"score": &graphql.Field{
				Type: graphql.Int,
			},
		},
	})

	rootQuery := graphql.NewObject(graphql.ObjectConfig{
		Name: "RootQuery",
		Fields: graphql.Fields{
			"players": &graphql.Field{
				Type: graphql.NewList(playerType),
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					return players, nil
				},
			},
		},
	})

	mutation := graphql.NewObject(graphql.ObjectConfig{
		Name: "Mutation",
		Fields: graphql.Fields{
			"updateScore": &graphql.Field{
				Type: playerType,
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{
						Type: graphql.NewNonNull(graphql.String),
					},
					"score": &graphql.ArgumentConfig{
						Type: graphql.NewNonNull(graphql.Int),
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					id := p.Args["id"].(string)
					score := p.Args["score"].(int)

					for i := 0; i < len(players); i++ {
						if players[i].Id == id {
							players[i].Score = score
							publishingChannel <- id
							return players[i], nil
						}
					}
					return nil, errors.New("Player id not found")
				},
			},
		},
	})

	subscription := graphql.NewObject(graphql.ObjectConfig{
		Name: "Subscription",

		Fields: graphql.Fields{
			"subscribeToPlayer": &graphql.Field{
				Type: playerType,
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{
						Type: graphql.NewNonNull(graphql.String),
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					id := p.Args["id"].(string)

					for i := 0; i < len(players); i++ {
						if players[i].Id == id {
							return players[i], nil
						}
					}
					return nil, errors.New("Player id not found")
				},
			},
		},
	})

	schemaConfig := graphql.SchemaConfig{
		Query:        rootQuery,
		Mutation:     mutation,
		Subscription: subscription,
	}
	schema, err := graphql.NewSchema(schemaConfig)
	if err != nil {
		log.Fatalf("failed to create graphql schema, err: %v\n", err)
	}
	graphqlHandler := handler.New(&handler.Config{
		Schema:     &schema,
		Pretty:     true,
		GraphiQL:   true,
		Playground: true,
	})
	http.Handle("/graphql", CorsMiddleWare(graphqlHandler))
	http.HandleFunc("/subscriptions", func(writer http.ResponseWriter, request *http.Request) {
		conn, err := upgrader.Upgrade(writer, request, nil)
		if err != nil {
			log.Printf("Failes to do websocket upgrade, err: %v\n", err)
		}
		connectionACK, err := json.Marshal(map[string]string{
			"type": "connection_ack",
		})
		if err != nil {
			log.Printf("failed to marshal ws connection ack, err: %v\n", err)
		}

		if err := conn.WriteMessage(websocket.TextMessage, connectionACK); err != nil {
			log.Printf("Failed to write to ws connection: %v\n", err)
			return
		}
		go func() {
			for {
				_, p, err := conn.ReadMessage()
				if websocket.IsCloseError(err, websocket.CloseGoingAway) {
					return
				}
				if err != nil {
					log.Printf("Failed to read websocket message, err: %v\n", err)
					return
				}
				var msg ConnectionACKMessage
				if err := json.Unmarshal(p, &msg); err != nil {
					log.Printf("Failed to unmarshal ConnectionAckMessage, err: %v\n", err)
					return
				}

				if msg.Type == "start" {
					length := 0
					subscribers.Range(func(key, value interface{}) bool {
						length++
						return true
					})
					var subscriber = Subscriber{
						ID:            length + 1,
						Conn:          conn,
						RequestString: msg.Payload.Query,
						OperationID:   msg.OperationID,
					}
					subscribers.Store(subscriber.ID, &subscriber)
				}
			}
		}()
	})

	go func() {
		for {
			playerId := <- publishingChannel
			publishUpdate(playerId, subscribers, schema)
		}
	}()

	const port = ":5000"
	fmt.Println("Listening at port: ", port)
	log.Fatal(http.ListenAndServe(port, nil))
}

func CorsMiddleWare(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Max-Age", "86400")
		w.Header().Set("Access-Control-Allow-Methods", "POST, PATCH, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, api_key, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		w.Header().Set("Access-Control-Expose-Headers", "Content-Length")
		w.Header().Set("Access-Control-Allow-Credentials", "true")

		handler.ServeHTTP(w, r)
	})
}

func publishUpdate(id string, subscribers sync.Map, schema graphql.Schema) {
	subscribers.Range(func(key, value interface{}) bool {
		subscriber, ok := value.(*Subscriber)
		if !ok {
			return true
		}
		payload := graphql.Do(graphql.Params{
			Schema:        schema,
			RequestString: subscriber.RequestString,
		})
		message, err := json.Marshal(map[string]interface{}{
			"type":    "data",
			"id":      subscriber.OperationID,
			"payload": payload,
		})
		if err != nil {
			log.Printf("failed to marhshal payload, err: %v\n", err)
			return true
		}
		if err := subscriber.Conn.WriteMessage(websocket.TextMessage, message); err != nil {
			if err == websocket.ErrCloseSent {
				subscribers.Delete(key)
				return true
			}
			log.Printf("failed to write to ws connection: %v\n", err)
			return true
		}
		return true
	})
}
